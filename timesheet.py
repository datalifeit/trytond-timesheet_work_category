# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelSQL
from trytond.pool import PoolMeta
from trytond.transaction import Transaction


class Work(metaclass=PoolMeta):
    __name__ = 'timesheet.work'

    categories = fields.Many2Many('timesheet.work-work.category',
        'work', 'category', 'Categories')

    @staticmethod
    def default_categories():
        return Transaction().context.get('categories', [])


class WorkCategory(ModelSQL):
    'Party - Category'
    __name__ = 'timesheet.work-work.category'
    _table = 'work_category_rel'

    work = fields.Many2One('timesheet.work', 'Work', ondelete='CASCADE',
            required=True, select=True)
    category = fields.Many2One('timesheet.work.category', 'Category',
        ondelete='CASCADE', required=True, select=True)
