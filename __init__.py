# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import category
from . import timesheet


def register():
    Pool.register(
        category.Category,
        timesheet.Work,
        timesheet.WorkCategory,
        module='timesheet_work_category', type_='model')
